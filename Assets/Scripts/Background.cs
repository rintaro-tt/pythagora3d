﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public GameObject goBall;

    void Update()
    {
        float y = transform.position.y;
        if (goBall.transform.position.y - transform.position.y < 0) {
            y = goBall.transform.position.y;
        }
        transform.position = new Vector3(
            goBall.transform.position.x,
            y,
            goBall.transform.position.z
        );
    }
}
