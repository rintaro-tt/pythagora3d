﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public GameObject goBall;
    public GameObject goCenter;
    public GameObject goStage;
    public GameObject goBackground;
    public GameObject goTitle;
    public GameObject goTitleStageText;
    public GameObject goStatusBar;
    public GameObject goStageText;
    public GameObject goNextStageText;
    public GameObject goStarText;
    public GameObject goStageClear;
    public GameObject goGameOver;
    public GameObject goGameData;
    public GameObject goFireworks;
    public GameObject goFire;
    public GameObject goRainbow;
    public GameObject[] goGoals = new GameObject[4];
    public GameObject goScrollbar;

    public GameObject goSound;
    public GameObject goVibe;
    public Material bgMaterial;

    // タッチ可能か判定
    private bool touchState = false;

    private bool titleState = true;
    private bool isSwipeStart = false;    
    private Vector3 touchStartPos;

    private Rigidbody rigidBall;
    private Stage stage;
    private JsonGameInfo gameInfo;
    private bool clearState = false;
    private Ball ball;

    void Start()
    {
        StartCoroutine(Init());
    }

    private IEnumerator Init()
    {
        goGameData.GetComponent<GameData>().LoadGameInfo();
        yield return new WaitForSeconds(0.1f);
        gameInfo = goGameData.GetComponent<GameData>().GetGameInfo();
        goSound.GetComponent<Sound>().Init(gameInfo.sound);
        goVibe.GetComponent<Vibe>().Init(gameInfo.vibe);

        stage = goStage.GetComponent<Stage>();
        stage.CreateStage(gameInfo.stageNum); 
        rigidBall = goBall.GetComponent<Rigidbody>();
        rigidBall.isKinematic = false;
        touchState = true;
        SetStageText();
        SetStarText();
        goTitleStageText.GetComponent<Text>().text = gameInfo.stageNum.ToString();
        ball = goBall.GetComponent<Ball>();
        bgMaterial.color = new Color(bgMaterial.color.r, bgMaterial.color.g, bgMaterial.color.b, 1);
    }

    void Update()
    {
        if (touchState) {
            GameOver();
            TouchAction();
        }
    }

    private void TouchAction() 
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0)) {
            TouchStart();
        }
        if (Input.GetMouseButtonUp(0)) {
            TouchEnd();
        }
        if (Input.GetMouseButton(0)) {
            TouchMoved();
        }
#else
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) {
                TouchStart();
            }
            if (touch.phase == TouchPhase.Ended) {
                TouchEnd();
            }
            if (touch.phase == TouchPhase.Moved) {
                TouchMoved();
            }
        }
#endif
    }

    private void TouchStart()
    {
        touchStartPos = Input.mousePosition;
        isSwipeStart = true;
    }

    private void TouchEnd()
    {
        goStage.transform.parent = null;
        goCenter.transform.rotation =  Quaternion.identity;

        goStage.transform.parent = null;
        isSwipeStart = false;
    }

    private void TouchMoved()
    {
        if (isSwipeStart) {
            goCenter.transform.position = new Vector3(
                goBall.transform.position.x,  
                goBall.transform.position.y,    
                goBall.transform.position.z
            );
            goStage.transform.parent = goCenter.transform;

            float x = Input.mousePosition.x - touchStartPos.x;
            float y = Input.mousePosition.y - touchStartPos.y;

            Vector3 rot = new Vector3(-y*Const.ROTATE_POWER, 0, x*Const.ROTATE_POWER);
            if (goStage.transform.rotation.x > Const.TILT_ANGLE && rot.x > 0) rot.x = 0; 
            if (goStage.transform.rotation.x < -Const.TILT_ANGLE && rot.x < 0) rot.x = 0; 
            if (goStage.transform.rotation.z > Const.TILT_ANGLE && rot.z > 0) rot.z = 0; 
            if (goStage.transform.rotation.z < -Const.TILT_ANGLE && rot.z < 0) rot.z = 0; 

            goCenter.transform.Rotate(rot.x, 0, rot.z);

            goStage.transform.parent = null;
            goCenter.transform.rotation =  Quaternion.identity;

            touchStartPos = Input.mousePosition;
        }
    }

    public void TouchTitle()
    {
        if (titleState) {
            goTitle.SetActive(false);
            goStatusBar.SetActive(true);
            titleState = false;
        }
    }

    private void GameOver()
    {
        if (ball.CheckGameOver()) {
            StartCoroutine(GameOverCoroutine());
        }
    }

    private IEnumerator GameOverCoroutine()
    {
        touchState = false;
        StopBall();
        rigidBall.isKinematic = true;
        goBall.transform.Find("Explosion").gameObject.SetActive(true);
        goBall.GetComponent<MeshRenderer>().enabled = false;
        goBall.transform.Find("Trail").gameObject.GetComponent<TrailRenderer>().enabled = false;
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_GAMEOVER);
        goVibe.GetComponent<Vibe>().OnVibe();
        yield return new WaitForSeconds(0.75f);

        goStatusBar.SetActive(false);
        goGameOver.SetActive(true);

    }

    public void Restart()
    {
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_BUTTON);
        goBall.transform.Find("Explosion").gameObject.SetActive(false);
        StopBall();
        goBall.GetComponent<MeshRenderer>().enabled = true;
        goBall.transform.Find("Trail").gameObject.GetComponent<TrailRenderer>().enabled = true;
        rigidBall.isKinematic = false;
        goStage.transform.position = new Vector3(0,0,0);
        goStage.transform.rotation = Quaternion.identity;
        goBackground.transform.position = new Vector3(0,0,0);
        goBall.transform.position = new Vector3(0,0,0);
        goBall.transform.Find("Trail").gameObject.GetComponent<TrailRenderer>().Clear();
        goGameOver.SetActive(false);
        titleState = true;
        stage.DestroyStage(); 
        stage.CreateStage(gameInfo.stageNum); 
        SetStageText();
        goTitleStageText.GetComponent<Text>().text = gameInfo.stageNum.ToString();
        SetStarText();
        goTitle.SetActive(true);
        goStatusBar.SetActive(false);
        goStageClear.SetActive(false);
        touchState = true;
        clearState = false; 
    }

    public void StageStart()
    {
        if (!touchState) {
            StopBall();
        }
        touchState = true;
    }

    public void StageClear(Vector3 goalPos)
    {
        if (!clearState) {
            clearState = true;
            StartCoroutine(StageClearCoroutine(goalPos));
        }
    }

    private IEnumerator StageClearCoroutine(Vector3 goalPos)
    {
        int n, m;

        GameObject go = Instantiate(goFire, new Vector3(
                goBall.transform.position.x,
                goBall.transform.position.y,
                goBall.transform.position.z),Quaternion.identity) as GameObject;
        Destroy(go.gameObject,3);

        StartCoroutine(Rainbow());

        goVibe.GetComponent<Vibe>().OnVibe();
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_GOAL);

        gameInfo.stageNum++;
        goGameData.GetComponent<GameData>().SetGameInfo(gameInfo);
        goGameData.GetComponent<GameData>().SaveGameInfo();

        rigidBall.isKinematic = true;
        touchState = false;
        StopBall();
        goStatusBar.SetActive(false);

        n = 0;
        while (n < 4) {
            goGoals[n].GetComponent<RectTransform>().localScale = new Vector3(0,0,0);
            n++;
        }

        goStageClear.SetActive(true);

        StopBall();

        // ボールをゴールの中心に移動させる
        goBall.transform.position = new Vector3(goalPos.x, goalPos.y, goalPos.z);

        // ステージを回転
        goCenter.transform.position = new Vector3(0,0,0);
        goCenter.transform.position = new Vector3(goalPos.x, goalPos.y, goalPos.z);
        goStage.transform.parent = goCenter.transform;
        goCenter.transform.rotation = new Quaternion(
            -goStage.transform.rotation.x, -goStage.transform.rotation.y, -goStage.transform.rotation.z, 1);
        goStage.transform.parent = null;

        n = 0;
        while (n < 4) {
            m = 0;
            while (m < 5) {
                goGoals[n].GetComponent<RectTransform>().localScale = new Vector3((m+1f)/5f,(m+1f)/5f,(m+1f)/5f);
                m++;
                yield return new WaitForSeconds(1/30f);
            }
            n++;
            yield return new WaitForSeconds(1/30f);
        }
    }

    private IEnumerator Rainbow()
    {
        yield return new WaitForSeconds(1);

        GameObject go = Instantiate(goRainbow, new Vector3(
                goBall.transform.position.x,
                goBall.transform.position.y,
                goBall.transform.position.z),Quaternion.identity) as GameObject;
        Destroy(go.gameObject,3);
    }

    public void NextStage()
    {
        StartCoroutine(NextStageCoroutine());
    }

    private IEnumerator NextStageCoroutine()
    {
        int n;

        GameObject go = Instantiate(goFireworks,
            new Vector3(goBall.transform.position.x,goBall.transform.position.y,goBall.transform.position.z), Quaternion.identity) as GameObject;
        Destroy(go.gameObject, 3);

        goVibe.GetComponent<Vibe>().OnVibe();
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_BUTTON);

        goStageClear.SetActive(false);
        goStatusBar.SetActive(true);

        rigidBall.isKinematic = false;
        rigidBall.constraints = (RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ);
        rigidBall.AddForce(new Vector3(0, Const.NEXT_STAGE_ADD_FORCE, 0));
        
        n = 0;
        while (n < (int)(Const.NEXT_STAGE_WAIT_TIME*Const.FPS)) {
            bgMaterial.color = new Color(bgMaterial.color.r, bgMaterial.color.g, bgMaterial.color.b,
                1 - n/(Const.NEXT_STAGE_WAIT_TIME*Const.FPS));
            n++;
            yield return new WaitForSeconds(1/(Const.NEXT_STAGE_WAIT_TIME*Const.FPS));
        }

        rigidBall.constraints = RigidbodyConstraints.None;
        stage.DestroyStage(); 
        goStage.transform.rotation = Quaternion.identity;
        goStage.transform.position = new Vector3(
            goBall.transform.position.x,
            goStage.transform.position.y,
            goBall.transform.position.z
        ); 
        stage.CreateStage(gameInfo.stageNum);
        SetStageText();
        goScrollbar.GetComponent<Scrollbar>().size = 0.0f;

        clearState = false; 

        goBackground.transform.position = new Vector3(0,0,0);
        n = 0;
        while (n < (int)(Const.NEXT_STAGE_WAIT_TIME*Const.FPS)) {
            bgMaterial.color = new Color(bgMaterial.color.r, bgMaterial.color.g, bgMaterial.color.b,
                n/(Const.NEXT_STAGE_WAIT_TIME*Const.FPS));
            n++;
            yield return new WaitForSeconds(1/(Const.NEXT_STAGE_WAIT_TIME*Const.FPS));
        }
    }

    private void SetStageText()
    {
        goStageText.GetComponent<Text>().text = gameInfo.stageNum.ToString();
        goNextStageText.GetComponent<Text>().text = (gameInfo.stageNum + 1).ToString();
    }

    public void GetStar()
    {
        gameInfo.starNum++;
        goGameData.GetComponent<GameData>().SetGameInfo(gameInfo);
        goGameData.GetComponent<GameData>().SaveGameInfo();
        SetStarText();
    }

    public void SetStarText()
    {
        goStarText.GetComponent<Text>().text = gameInfo.starNum.ToString();
    }

    public void StopBall()
    {
        rigidBall.velocity = Vector3.zero;
        rigidBall.angularVelocity= Vector3.zero;
    }

    public void ResetGameData()
    {
        gameInfo.stageNum = 1;
        gameInfo.starNum = 0;
        goGameData.GetComponent<GameData>().SetGameInfo(gameInfo);
        goGameData.GetComponent<GameData>().SaveGameInfo();
        Restart();
    }
}
