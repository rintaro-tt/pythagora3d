﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour
{
    public GameObject goBall;

    void Update()
    {
        transform.position = new Vector3(
            goBall.transform.position.x,
            goBall.transform.position.y + Const.CAMERA_RANGE,
            goBall.transform.position.z + Const.CAMERA_RANGE
        );
    }
}
