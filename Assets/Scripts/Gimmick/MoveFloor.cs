﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFloor : MonoBehaviour
{
    public int dir = Const.DIR_UP; // 上下左右前後6方向 
    public int type = Const.TYPE_MOVE; // ボールが触れてから動くか 
    public float length = 2.5f; // 移動距離
    public float wait = 1f; // 頂点で止まる時間
    public float speed = 0.5f; // 何秒で移動するか

    private Vector3 pos;

    void Start()
    {
        if (type == Const.TYPE_MOVE) {
            StartCoroutine(MoveCotourine());
        }
    }

    private IEnumerator MoveCotourine()
    {   
        int n;
        Vector3 dr = new Vector3(0,0,0);

        if (dir == Const.DIR_UP) {
            dr.y = 1;
        } else if (dir == Const.DIR_DOWN) {
            dr.y = -1;
        } else if (dir == Const.DIR_LEFT) {
            dr.z = -1;
        } else if (dir == Const.DIR_RIGHT) {
            dr.z = 1;
        } else if (dir == Const.DIR_FRONT) {
            dr.x = -1;
        } else if (dir == Const.DIR_BACK) {
            dr.x = 1;
        }

        yield return new WaitForSeconds(wait);

        n = 0;
        while (n < (int)(Const.FPS*speed)) {
            transform.localPosition = new Vector3(
                transform.localPosition.x + dr.x*length/(Const.FPS*speed),
                transform.localPosition.y + dr.y*length/(Const.FPS*speed),
                transform.localPosition.z + dr.z*length/(Const.FPS*speed)
            );
        
            n++;
            yield return new WaitForSeconds(1/Const.FPS);
        }
        yield return new WaitForSeconds(wait);

        n = 0;
        while (n < (int)(Const.FPS*speed)) {
            transform.localPosition = new Vector3(
                transform.localPosition.x - dr.x*length/(Const.FPS*speed),
                transform.localPosition.y - dr.y*length/(Const.FPS*speed),
                transform.localPosition.z - dr.z*length/(Const.FPS*speed)
            );
        
            n++;
            yield return new WaitForSeconds(1/Const.FPS);
        }

        StartCoroutine(MoveCotourine());
    }

    void OnTriggerEnter (Collider other)
    {
        if (type == Const.TYPE_STOP && other.gameObject.name == "Ball") {
            type = Const.TYPE_MOVE;
            StartCoroutine(MoveCotourine());
        }
    }
}
