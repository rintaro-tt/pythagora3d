﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFloor : MonoBehaviour
{
    public float wait = 1.5f; // 止まる時間

    private float waitTime = 0;
    private bool waitState = true;
    private int count = 1;

    void FixedUpdate()
    {
        if (waitState) {
            waitTime += Time.deltaTime;
            if (waitTime > wait) {
                waitState = false;
            } else {
               return;
            }
        } else {
            count++;
            if (count >= 90) {
                waitTime = 0;
                count = 1;
                waitState = true;
            } 
        }
        transform.Rotate(0, 1, 0);
    }
}
