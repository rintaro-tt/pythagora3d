﻿using UnityEngine;
using System.Collections;

public class TenjinScript : MonoBehaviour {

  private string apiKey = "T2WDDB18C9FSBMDZACZURGZ2ZUCYYQRZ";

  // Use this for initialization
  void Start () {

    BaseTenjin instance = Tenjin.getInstance(apiKey);
    instance.Connect();
  }

  // Update is called once per frame
  void Update () {

  }

  void OnApplicationPause(bool pauseStatus){
    if(pauseStatus){
      //do nothing
    }
    else
    {
      BaseTenjin instance = Tenjin.getInstance(apiKey);
      instance.Connect();
    }
  }
}
