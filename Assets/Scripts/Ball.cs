﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public GameObject goController;
    public GameObject goScrollbar;
    public GameObject goStage;
    public GameObject goBackground;
    public GameObject goStarEffect;
    public GameObject goSound;
    public GameObject goPlusOne;

    private SphereCollider sCollider;
    private bool startState = false;
    private Vector3 startPos;
    private Vector3 goalPos;
    private float distance;
    private Rigidbody rigid;

    // 壁抜け防止
    void Start()
    {
        sCollider = GetComponent<SphereCollider>();
        goScrollbar.GetComponent<Scrollbar>().size = 0.0f;
        distance = Vector3.Distance(transform.position,goalPos);
        rigid = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (startState) {
            if (distance > Vector3.Distance(transform.position,goalPos)) {
                goScrollbar.GetComponent<Scrollbar>().size
                    = Vector3.Distance(transform.position,startPos)/Vector3.Distance(goalPos,startPos);
            } else {
                goScrollbar.GetComponent<Scrollbar>().size = 0;
            }
        }
    }
    
    void FixedUpdate()
    {
        sCollider.enabled = false;
        sCollider.enabled = true;
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.name == "Start") {
            startState = true;
            startPos = new Vector3(transform.position.x,transform.position.y,transform.position.z);
            GameObject go = goStage.transform.Find("Stage").gameObject.transform.Find("Goal").gameObject;
            goalPos = new Vector3(go.transform.position.x,go.transform.position.y,go.transform.position.z);
            distance = Vector3.Distance(transform.position,goalPos);
            goController.GetComponent<Controller>().StageStart();
        }
        if (other.gameObject.name == "Goal") {
            startState = false;
            goController.GetComponent<Controller>().StageClear(other.gameObject.transform.position);
        }
        if (other.gameObject.name == "Star") {
            goController.GetComponent<Controller>().GetStar();

            goSound.GetComponent<Sound>().OnSound(Const.SOUND_STAR);
            GameObject go = Instantiate(goStarEffect,
                new Vector3(transform.position.x,transform.position.y,transform.position.z), Quaternion.identity) as GameObject;
            Destroy(go.gameObject, 1);

            GameObject go2 = Instantiate(goPlusOne, new Vector3(
                other.gameObject.transform.position.x,
                other.gameObject.transform.position.y,
                other.gameObject.transform.position.z), Quaternion.identity) as GameObject;
            go2.transform.rotation = Quaternion.Euler(0,180,0);
            Destroy(go2.gameObject, 1);
            Destroy(other.gameObject);
        }
    }

    public bool CheckGameOver()
    {
        bool state = false;

        if (rigid.velocity.y < Const.GAMEOVER_VELOCITY
            && transform.position.y - startPos.y < Const.GAMEOVER_POSY && transform.position.y - goalPos.y < Const.GAMEOVER_POSY) {
            state = true;
        }
        return state;
    }
}
