﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    public GameObject goBall;

    // ボールの下からテイルを出すための処理
    void Update()
    {
        transform.position = new Vector3(
            goBall.transform.position.x,
            goBall.transform.position.y - 0.5f,
            goBall.transform.position.z
            );
    }
}
