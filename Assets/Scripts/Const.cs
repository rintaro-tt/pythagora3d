﻿public class Const
{
    public const int CAMERA_RANGE   = 18;  
    public const float ROTATE_POWER = 0.05f;
    public const float TILT_ANGLE   = 0.5f;
        
    public const float NEXT_STAGE_ADD_FORCE = 3000f;    
    public const float NEXT_STAGE_WAIT_TIME = 2f;
        
    public const float FPS = 60f;

    public const float GAMEOVER_VELOCITY = -80f;
    public const float GAMEOVER_POSY     = -10f;

    public const int SOUND_BGM      = 0;
    public const int SOUND_BUTTON   = 1;
    public const int SOUND_STAR     = 2;
    public const int SOUND_GOAL     = 3;
    public const int SOUND_GAMEOVER = 4;
    public const int SOUND_MAX      = 5;

    public const int DIR_UP    = 0;    
    public const int DIR_DOWN  = 1;    
    public const int DIR_LEFT  = 2;    
    public const int DIR_RIGHT = 3;    
    public const int DIR_FRONT = 4;    
    public const int DIR_BACK  = 5;
    
    public const int TYPE_MOVE = 0;    
    public const int TYPE_STOP = 1;    
}
