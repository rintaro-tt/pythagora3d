﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Info : MonoBehaviour
{
    public GameObject goSound;
    public string privacyPolicyUrl = "https://gyi.spiritduck.be/privacy/";

    public void ShowPrivacyPolicy()
    {
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_BUTTON);
        Application.OpenURL(privacyPolicyUrl);
    }
}
