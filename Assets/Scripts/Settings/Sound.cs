﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public GameObject goGameData;
    public GameObject goSoundOn;
    public GameObject goSoundOff;
    public GameObject goSoundOn2;
    public GameObject goSoundOff2;

    private bool soundState;

    private AudioSource audioSource;
    public AudioClip[] audioClip = new AudioClip[Const.SOUND_MAX];

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audioClip[Const.SOUND_BGM];
        audioSource.Play ();
    }

    public void Init(bool state)
    {
        soundState = state;
        if (soundState) {
            goSoundOn.SetActive(true);
            goSoundOff.SetActive(false);
            goSoundOn2.SetActive(true);
            goSoundOff2.SetActive(false);
        } else {
            goSoundOn.SetActive(false);
            goSoundOff.SetActive(true);
            goSoundOn2.SetActive(false);
            goSoundOff2.SetActive(true);
        }
    }

    public void SetSound()
    {
        OnSound(Const.SOUND_BUTTON);

        JsonGameInfo gameInfo = goGameData.GetComponent<GameData>().GetGameInfo();
        if (soundState) {
            soundState = false;
            gameInfo.sound = false;
            goSoundOn.SetActive(false);
            goSoundOff.SetActive(true);
            goSoundOn2.SetActive(false);
            goSoundOff2.SetActive(true);
            audioSource.volume = 0;
        } else {
            soundState = true;
            gameInfo.sound = true;
            goSoundOn.SetActive(true);
            goSoundOff.SetActive(false);
            goSoundOn2.SetActive(true);
            goSoundOff2.SetActive(false);
            audioSource.volume = 1;
        }
        goGameData.GetComponent<GameData>().SetGameInfo(gameInfo);
        goGameData.GetComponent<GameData>().SaveGameInfo();
    }

    public void OnSound(int n = Const.SOUND_BUTTON)
    {
        audioSource.PlayOneShot(audioClip[n]);
    }
}
