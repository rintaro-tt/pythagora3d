﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vibe : MonoBehaviour
{
    public GameObject goGameData;
    public GameObject goVibeOn;
    public GameObject goVibeOff;
    public GameObject goVibeOn2;
    public GameObject goVibeOff2;
    public GameObject goSound;

    private bool vibeState;

    public void Init(bool state)
    {
        vibeState = state;
        if (vibeState) {
            goVibeOn.SetActive(true);
            goVibeOff.SetActive(false);
            goVibeOn2.SetActive(true);
            goVibeOff2.SetActive(false);
        } else {
            goVibeOn.SetActive(false);
            goVibeOff.SetActive(true);
            goVibeOn2.SetActive(false);
            goVibeOff2.SetActive(true);
        }
    }

    public void SetVibe()
    {
        goSound.GetComponent<Sound>().OnSound(Const.SOUND_BUTTON);

        JsonGameInfo gameInfo = goGameData.GetComponent<GameData>().GetGameInfo();
        if (vibeState) {
            vibeState = false;
            gameInfo.vibe = false;
            goVibeOn.SetActive(false);
            goVibeOff.SetActive(true);
            goVibeOn2.SetActive(false);
            goVibeOff2.SetActive(true);
        } else {
            vibeState = true;
            gameInfo.vibe = true;
            goVibeOn.SetActive(true);
            goVibeOff.SetActive(false);
            goVibeOn2.SetActive(true);
            goVibeOff2.SetActive(false);
        }
        goGameData.GetComponent<GameData>().SetGameInfo(gameInfo);
        goGameData.GetComponent<GameData>().SaveGameInfo();
    }

    public void OnVibe()
    {
        if (vibeState) {
            if (SystemInfo.supportsVibration) Handheld.Vibrate();
        }
    }
}
