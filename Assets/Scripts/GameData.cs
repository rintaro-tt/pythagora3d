﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class GameData : MonoBehaviour
{
    private JsonGameInfo jsonGameInfo;
    private string gameInfoPath;

    void Start()
    {
        gameInfoPath = Application.persistentDataPath + "/gameInfo.json";    
    }

    public void SetGameInfo (JsonGameInfo gameInfo)
    {
        jsonGameInfo = gameInfo;
    }

    public JsonGameInfo GetGameInfo ()
    {
        return jsonGameInfo;
    }

    public void LoadGameInfo ()
    {
        if (File.Exists(gameInfoPath)) {
            FileStream f = new FileStream(gameInfoPath, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(f);
            string json = reader.ReadString();
            jsonGameInfo = JsonUtility.FromJson<JsonGameInfo>(json);
            reader.Close();
        } else {
            jsonGameInfo = new JsonGameInfo();
        }
    }

    public void SaveGameInfo ()
    {
        string text = JsonUtility.ToJson(jsonGameInfo);
        FileStream f = new FileStream(gameInfoPath, FileMode.Create, FileAccess.Write);
        BinaryWriter writer = new BinaryWriter(f);
        writer.Write(text);
        writer.Close();
    }

    public void DeleteGameInfo ()
    {
        if (File.Exists(gameInfoPath)) {
            File.Delete(gameInfoPath);
        }
    }
}

[Serializable]
public class JsonGameInfo {
    public int stageNum = 1;
    public int starNum = 0;
    public bool sound = true;
    public bool vibe = true;
}
