﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public void CreateStage(int stageNum)
    {
        // とりあえず
        stageNum = (stageNum - 1)%8 + 1;


        GameObject go = (GameObject)Resources.Load ("Prefabs/Stage/Stage" + stageNum);
        GameObject goStage = Instantiate(go) as GameObject;
        goStage.name = "Stage";
        goStage.transform.parent = transform;
        goStage.transform.localPosition = new Vector3(0,0,0);
        goStage.transform.position = new Vector3(goStage.transform.position.x,0,goStage.transform.position.z);
        transform.rotation = Quaternion.identity;
        goStage.transform.rotation = Quaternion.identity;
    }

    public void DestroyStage()
    {
        foreach (Transform n in transform) {
            GameObject.Destroy(n.gameObject);
        }
    }
}
